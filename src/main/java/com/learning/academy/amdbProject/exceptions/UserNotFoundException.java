package com.learning.academy.amdbProject.exceptions;

public class UserNotFoundException extends RuntimeException {

    private String exceptionMessage;

    public UserNotFoundException(String message) {
        super(message);
    }

    public UserNotFoundException(String message, Throwable cause, String exceptionMessage) {
        super(message, cause);
        this.exceptionMessage = exceptionMessage;
    }

    public UserNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
