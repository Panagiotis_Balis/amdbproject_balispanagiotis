package com.learning.academy.amdbProject.exceptions;

public class MovieNotFoundException extends RuntimeException {

    private String exceptionMessage;

    public MovieNotFoundException(String message) {
        super(message);
    }

    public MovieNotFoundException(String message, Throwable cause, String exceptionMessage) {
        super(message, cause);
        this.exceptionMessage = exceptionMessage;
    }

    public MovieNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
