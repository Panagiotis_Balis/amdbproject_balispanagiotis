package com.learning.academy.amdbProject.exceptions.Errors;

public class VoteError {

    private String message;
    private int code;

    public VoteError() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public VoteError(String message, int code) {
        this.message = message;
        this.code = code;
    }
}