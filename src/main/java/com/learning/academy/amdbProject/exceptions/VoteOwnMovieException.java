package com.learning.academy.amdbProject.exceptions;

public class VoteOwnMovieException extends RuntimeException {
    private String exceptionMessage;

    public VoteOwnMovieException(String message) {
        super(message);
    }

    public VoteOwnMovieException(String message, Throwable cause, String exceptionMessage) {
        super(message, cause);
        this.exceptionMessage = exceptionMessage;
    }

    public VoteOwnMovieException(String message, Throwable cause) {
        super(message, cause);
    }
}

