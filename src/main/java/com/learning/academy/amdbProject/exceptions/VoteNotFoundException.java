package com.learning.academy.amdbProject.exceptions;

public class VoteNotFoundException extends RuntimeException {

    private String exceptionMessage;

    public VoteNotFoundException(String message) {
        super(message);
    }

    public VoteNotFoundException(String message, Throwable cause, String exceptionMessage) {
        super(message, cause);
        this.exceptionMessage = exceptionMessage;
    }

    public VoteNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
