package com.learning.academy.amdbProject.domain;

import com.learning.academy.amdbProject.enums.UserVote;

import javax.persistence.*;

@Entity
@Table(name = "VOTE")

public class Vote {
    private static final int MAX_NAME_LENGTH = 60;


    @EmbeddedId
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private UserMovieKey id;

    @ManyToOne
    @MapsId("user_id")
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @MapsId("movie_id")
    @JoinColumn(name = "movie_id")
    private Movie movie;


    @Column(name = "vote_type")
    @Enumerated(EnumType.STRING)
    private UserVote userVote;

    public Vote() {
    }

    public Vote(UserMovieKey userMovieKey, Movie movie, User user, UserVote userVote) {
        this.id = userMovieKey;
        this.movie= movie;
        this.user = user;
        this.userVote = userVote;
    }

    public UserMovieKey getId() {
        return id;
    }

    public void setId(UserMovieKey id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public UserVote getUserVote() {
        return userVote;
    }

    public void setUserVote(UserVote userVote) {
        this.userVote = userVote;
    }
}
