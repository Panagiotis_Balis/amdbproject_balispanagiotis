package com.learning.academy.amdbProject.domain;

import com.learning.academy.amdbProject.dto.UserDTO;

import java.util.List;

public class Users {
    private List<UserDTO> myUsers;

    public Users(){
    }

    public List<UserDTO> getMyUsers() {
        return myUsers;
    }

    public void setMyUsers(List<UserDTO> myUsers) {
        this.myUsers = myUsers;
    }
}
