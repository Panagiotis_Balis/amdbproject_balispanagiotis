package com.learning.academy.amdbProject.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.learning.academy.amdbProject.enums.UserRole;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "USER",uniqueConstraints = {@UniqueConstraint(columnNames = {"user_name"})})
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "user_id")
public class User {
    private static final int MAX_NAME_LENGTH = 60;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Long id;

    @Column(name = "name", length = MAX_NAME_LENGTH)
    private String name;

    @Column(name = "user_name", nullable = false, length = MAX_NAME_LENGTH)
    private String username;

    @Column(name = "password", nullable = false, length = MAX_NAME_LENGTH)
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(name = "user_role")
    private UserRole userRole;


    public User() {
    }

    public User(List<Movie> movies, UserRole userRole, String name, String username, String password) {
        this.name = name;
        this.password = password;
        this.username = username;
        this.userRole = userRole;

    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }
}

