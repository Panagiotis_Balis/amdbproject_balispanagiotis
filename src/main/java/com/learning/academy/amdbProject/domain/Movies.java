package com.learning.academy.amdbProject.domain;

import com.learning.academy.amdbProject.dto.MovieDTO;

import java.util.List;

public class Movies {
    private List<MovieDTO> myMovies;

    public Movies(){

    }

    public List<MovieDTO> getMyMovies() {
        return myMovies;
    }

    public void setMyMovies(List<MovieDTO> myMovies) {
        this.myMovies = myMovies;
    }
}
