package com.learning.academy.amdbProject.domain;

import javax.persistence.*;
import java.time.LocalDate;


@Entity
@Table(name = "MOVIE", uniqueConstraints = {@UniqueConstraint(columnNames = {"movie_title"})})

public class Movie {
    private static final int MAX_NAME_LENGTH = 60;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "movie_id", nullable = false)
    private Long id;

    @ManyToOne(optional = false, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "movie_title", length = MAX_NAME_LENGTH, nullable = false)
    private String title;


    @Column(name = "movie_description", length = MAX_NAME_LENGTH)
    private String description;


    @Column(name = "created_at", updatable = false)
    private LocalDate createdAt;

    @Column(name = "number_of_likes", length = MAX_NAME_LENGTH)
    private Integer likes;

    @Column(name = "number_of_dislikes", length = MAX_NAME_LENGTH)
    private Integer dislikes;


    @Version
    @Column(name = "version")
    private Long version;

    static LocalDate currentTime() {
        return LocalDate.now();
    }

    @PrePersist
    public void onPrePersist() {
        if (createdAt == null) {
            createdAt = currentTime();
        }
    }

    //Default Constructor
    public Movie() {
    }

    public Movie(String title, String description, Integer likes, Integer dislikes, Long version) {
        this.description = description;
        this.likes = likes;
        this.dislikes = dislikes;
        this.title = title;
        this.version = version;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long movie_id) {
        this.id = movie_id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Integer getDislikes() {
        return dislikes;
    }

    public void setDislikes(Integer dislikes) {
        this.dislikes = dislikes;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Book{");
        sb.append("movie id= ").append(id).append('\'');
        sb.append(", title=").append(title).append('\'');
        sb.append(", description=").append(description).append('\'');
        sb.append(", user that submitted it=").append(user).append('\'');
        sb.append(", date of publication=").append(createdAt).append('\'');
        sb.append(", number of likes=").append(likes).append('\'');
        sb.append(", number if dislikes").append(dislikes).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
