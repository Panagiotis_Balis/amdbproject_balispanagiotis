package com.learning.academy.amdbProject.dto;

import com.learning.academy.amdbProject.enums.UserVote;

public class VoteDTO {
    //    private Long id;
    private Long userId;
    private Long movieId;
    private UserVote userVote;

    VoteDTO() {

    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getMovieId() {
        return movieId;
    }

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }


    public UserVote getUserVote() {
        return userVote;
    }

    public void setUserVote(UserVote userVote) {
        this.userVote = userVote;
    }
}
