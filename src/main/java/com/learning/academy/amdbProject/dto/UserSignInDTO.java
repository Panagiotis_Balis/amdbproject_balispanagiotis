package com.learning.academy.amdbProject.dto;

import javax.validation.constraints.NotNull;

public class UserSignInDTO {
    @NotNull
    private String username;
    @NotNull
    private String password;

    public UserSignInDTO(){

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
