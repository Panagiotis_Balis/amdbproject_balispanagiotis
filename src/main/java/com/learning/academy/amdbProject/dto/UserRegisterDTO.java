package com.learning.academy.amdbProject.dto;

import com.learning.academy.amdbProject.enums.UserRole;

import javax.validation.constraints.NotNull;

public class UserRegisterDTO {
    @NotNull
    private String name;
    @NotNull
    private String user_name;
    @NotNull
    private String password;
    private UserRole userRole;
    public UserRegisterDTO(){

    }


    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
