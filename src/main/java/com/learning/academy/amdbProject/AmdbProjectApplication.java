package com.learning.academy.amdbProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AmdbProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(AmdbProjectApplication.class, args);
	}

}
