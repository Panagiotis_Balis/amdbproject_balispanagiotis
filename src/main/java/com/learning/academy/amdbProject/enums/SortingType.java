package com.learning.academy.amdbProject.enums;

public enum SortingType {
    LIKES,
    DISLIKES,
    DATE
}
