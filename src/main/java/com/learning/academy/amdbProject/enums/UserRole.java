package com.learning.academy.amdbProject.enums;

public enum UserRole {
    ADMIN("administrator of WebApplication"),
    USER("a registered user");


    private String description;

    UserRole(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
