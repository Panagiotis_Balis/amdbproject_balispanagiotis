package com.learning.academy.amdbProject.enums;

public enum SortingOrder {
    ASC,
    DESC
}
