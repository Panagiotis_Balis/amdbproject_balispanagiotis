package com.learning.academy.amdbProject.service;

import com.learning.academy.amdbProject.converter.MovieConverter;
import com.learning.academy.amdbProject.domain.Movie;
import com.learning.academy.amdbProject.domain.Movies;
import com.learning.academy.amdbProject.domain.User;
import com.learning.academy.amdbProject.dto.MovieDTO;
import com.learning.academy.amdbProject.enums.SortingOrder;
import com.learning.academy.amdbProject.enums.SortingType;
import com.learning.academy.amdbProject.repository.MovieRepository;
import com.learning.academy.amdbProject.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MovieServiceImpl implements MovieService {
    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MovieConverter movieConverter;

    Movies movies = new Movies();

    @Override
    public Movies findMovies(SortingType sortingType, SortingOrder sortingOrder) {
        if (sortingType == null) {
            sortingType = SortingType.DATE;
        }
        if (sortingOrder == null) {
            sortingOrder = SortingOrder.DESC;
        }
        if (sortingType.equals(SortingType.DATE) && sortingOrder.equals(SortingOrder.ASC)) {
            movies.setMyMovies(movieRepository.findAllByOrderByCreatedAtAsc().
                    stream()
                    .map(movie -> movieConverter.convert(movie)).collect(Collectors.toList()));
            return movies;
        }
        if (sortingType.equals(SortingType.DATE) && sortingOrder.equals(SortingOrder.DESC)) {
            movies.setMyMovies(movieRepository.findAllByOrderByCreatedAtDesc().stream()
                    .map(movie -> movieConverter.convert(movie)).collect(Collectors.toList()));
            return movies;
        }
        if (sortingType.equals(SortingType.LIKES) && sortingOrder.equals(SortingOrder.DESC)) {
            movies.setMyMovies(movieRepository.findAllByOrderByLikesDesc().stream()
                    .map(movie -> movieConverter.convert(movie)).collect(Collectors.toList()));
            return movies;
        }
        if (sortingType.equals(SortingType.DISLIKES) && sortingOrder.equals(SortingOrder.DESC)) {
            movies.setMyMovies(movieRepository.findAllByOrderByDislikesDesc()
                    .stream()
                    .map(movie -> movieConverter.convert(movie))
                    .collect(Collectors.toList()));
            return movies;
        }
        if (sortingType.equals(SortingType.LIKES) && sortingOrder.equals(SortingOrder.ASC)) {
            movies.setMyMovies(movieRepository.findAllByOrderByLikesAsc()
                    .stream()
                    .map(movie -> movieConverter.convert(movie))
                    .collect(Collectors.toList()));
            return movies;
        }
        if (sortingType.equals(SortingType.DISLIKES) && sortingOrder.equals(SortingOrder.ASC)) {
            movies.setMyMovies(movieRepository.findAllByOrderByDislikesAsc().stream()
                    .map(movie -> movieConverter
                            .convert(movie)).collect(Collectors.toList()));
            return movies;
        } else
            movies.setMyMovies(movieRepository.findAll().stream()
                    .map(movie -> movieConverter.convert(movie))
                    .collect(Collectors.toList()));
        return movies;
    }

    @Override
    public Movies findMoviesByUser(Long id, SortingType sortingType, SortingOrder sortingOrder) {

        User user = userRepository.findUserById(id);

        if (sortingType == null) {
            sortingType = SortingType.DATE;
        }
        if (sortingOrder == null) {
            sortingOrder = SortingOrder.DESC;
        }
        if (sortingType.equals(SortingType.DATE) && sortingOrder.equals(SortingOrder.ASC)) {
            movies.setMyMovies(movieRepository.findAllMoviesByUser_IdOrderByCreatedAtAsc(id).
                    stream()
                    .map(movie -> movieConverter.convert(movie)).collect(Collectors.toList()));
            return movies;
        }
        if (sortingType.equals(SortingType.DATE) && sortingOrder.equals(SortingOrder.DESC)) {
            movies.setMyMovies(movieRepository.findMovieByUserOrderByCreatedAtDesc(user)
                    .stream()
                    .map(movie -> movieConverter.convert(movie))
                    .collect(Collectors.toList()));
            return movies;
        }
        if (sortingType.equals(SortingType.LIKES) && sortingOrder.equals(SortingOrder.DESC)) {
            movies.setMyMovies(movieRepository.findMoviesByUser_IdOrderByLikesDesc(id).stream()
                    .map(movie -> movieConverter.convert(movie)).collect(Collectors.toList()));
            return movies;
        }
        if (sortingType.equals(SortingType.DISLIKES) && sortingOrder.equals(SortingOrder.DESC)) {
            movies.setMyMovies(movieRepository.findMoviesByUser_IdOrderByDislikesDesc(id)
                    .stream()
                    .map(movie -> movieConverter.convert(movie))
                    .collect(Collectors.toList()));
            return movies;
        }
        if (sortingType.equals(SortingType.LIKES) && sortingOrder.equals(SortingOrder.ASC)) {
            movies.setMyMovies(movieRepository.findMoviesByUser_IdOrderByLikesAsc(id)
                    .stream()
                    .map(movie -> movieConverter.convert(movie))
                    .collect(Collectors.toList()));
            return movies;
        }
        if (sortingType.equals(SortingType.DISLIKES) && sortingOrder.equals(SortingOrder.ASC)) {
            movies.setMyMovies(movieRepository.findMoviesByUser_IdOrderByDislikesAsc(id).stream()
                    .map(movie -> movieConverter.convert(movie)).collect(Collectors.toList()));
            return movies;
        } else
            movies.setMyMovies(movieRepository.findAll().stream().map(movie -> movieConverter.convert(movie))
                    .collect(Collectors.toList()));
        return movies;
    }


    private List<MovieDTO> getMovies() {
        return movieRepository.findAll()
                .stream()
                .map(movie -> movieConverter.convert(movie))
                .collect(Collectors.toList());
    }

    @Override
    public MovieDTO saveMovie(MovieDTO movieDTO) {
        Movie movie = movieConverter.convert(movieDTO);
        Movie savedMovie = movieRepository.save(movie);//Movie with the given if from the database
        return movieConverter.convert(savedMovie);
    }

    @Override
    public MovieDTO findMovieDto(Long id) {
        MovieDTO movieDTO = movieConverter.convert(findMovieById(id));
        return movieDTO;
    }


    @Override
    public Movie findMovieById(Long id) {
        Optional<Movie> optionalMovie = movieRepository.findById(id);
        if (optionalMovie.get()==null) {
            return null;
        } else
            return optionalMovie.get();
    }
}