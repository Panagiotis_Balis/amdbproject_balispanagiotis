package com.learning.academy.amdbProject.service;

import com.learning.academy.amdbProject.converter.UserConverter;
import com.learning.academy.amdbProject.domain.User;
import com.learning.academy.amdbProject.dto.UserDTO;
import com.learning.academy.amdbProject.dto.UserRegisterDTO;
import com.learning.academy.amdbProject.dto.UserSignInDTO;
import com.learning.academy.amdbProject.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserConverter userConverter;

    @Override
    public User findUser(Long userId) {
        return userRepository.findUserById(userId);
    }

    @Override
    public User findUserByUserName(String userName) {
        return userRepository.findUserByUsername(userName);
    }

    @Override
    public UserDTO signInUser(UserSignInDTO userSignInDTO) {
        User user = findUserByUserName(userSignInDTO.getUsername());
        if (user != null & user.getPassword().equals(userSignInDTO.getPassword())) {
            UserDTO userDTO = userConverter.convert(userSignInDTO);
            return userDTO;
        } else {
            return null;
        }
    }

    @Override
    public void saveUser(UserRegisterDTO userRegisterDTO) {

        User user = userConverter.convert(userRegisterDTO);
        userRepository.save(user);

    }
}
