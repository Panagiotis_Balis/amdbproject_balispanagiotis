package com.learning.academy.amdbProject.service;

import com.learning.academy.amdbProject.domain.Movie;
import com.learning.academy.amdbProject.domain.Movies;
import com.learning.academy.amdbProject.dto.MovieDTO;
import com.learning.academy.amdbProject.enums.SortingOrder;
import com.learning.academy.amdbProject.enums.SortingType;

public interface MovieService {




    Movies findMovies(SortingType sortingType, SortingOrder sortingOrder);

    Movies findMoviesByUser(Long id,SortingType sortingType,SortingOrder sortingOrder);

    MovieDTO saveMovie(MovieDTO movieDTO);

    Movie findMovieById(Long id);

    MovieDTO findMovieDto(Long id);

}
