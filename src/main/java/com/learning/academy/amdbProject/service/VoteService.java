package com.learning.academy.amdbProject.service;

import com.learning.academy.amdbProject.domain.UserMovieKey;
import com.learning.academy.amdbProject.domain.Vote;
import com.learning.academy.amdbProject.dto.VoteDTO;

public interface VoteService {

    Vote vote(VoteDTO voteDTO);

    Vote delete(VoteDTO voteDTO);

    Vote findVote(UserMovieKey userMovieKey);

}
