package com.learning.academy.amdbProject.service;

import com.learning.academy.amdbProject.domain.User;
import com.learning.academy.amdbProject.dto.UserDTO;
import com.learning.academy.amdbProject.dto.UserRegisterDTO;
import com.learning.academy.amdbProject.dto.UserSignInDTO;

public interface UserService {


    User findUser(Long userId);

    User findUserByUserName(String userName);

    void saveUser(UserRegisterDTO userRegisterDTO);

    UserDTO signInUser(UserSignInDTO userSignInDTO);
}


