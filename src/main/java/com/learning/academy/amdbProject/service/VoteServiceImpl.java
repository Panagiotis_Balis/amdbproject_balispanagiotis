package com.learning.academy.amdbProject.service;

import com.learning.academy.amdbProject.converter.VoteConverter;
import com.learning.academy.amdbProject.domain.Movie;
import com.learning.academy.amdbProject.domain.UserMovieKey;
import com.learning.academy.amdbProject.domain.Vote;
import com.learning.academy.amdbProject.dto.VoteDTO;
import com.learning.academy.amdbProject.enums.UserVote;
import com.learning.academy.amdbProject.exceptions.VoteNotFoundException;
import com.learning.academy.amdbProject.exceptions.VoteOwnMovieException;
import com.learning.academy.amdbProject.repository.VoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class VoteServiceImpl implements VoteService {

    @Autowired
    private VoteRepository voteRepository;
    @Autowired
    private MovieService movieService;
    @Autowired
    private UserService userService;

    @Autowired
    private VoteConverter voteConverter;

    public Vote findVote(UserMovieKey userMovieKey) {
        Optional<Vote> voteOptional = voteRepository.findById(userMovieKey);
        if (voteOptional.get()==null) {
            return null;
        } else
            return voteOptional.get();
    }

    @Transactional
    public Vote delete(VoteDTO voteDTO) {
        UserMovieKey userMovieKey = new UserMovieKey(voteDTO.getUserId(), voteDTO.getMovieId());
        Vote vote = findVote(userMovieKey);
        if (vote != null) {
            Movie movie = movieService.findMovieById(voteDTO.getMovieId());
            if (voteDTO.getUserVote() == UserVote.DOWNVOTE) {
                movie.setDislikes(movie.getDislikes() - 1);
            } else {
                movie.setLikes(movie.getLikes() - 1);
            }
            voteRepository.delete(vote);
        }
        return vote;
    }

    @Transactional
    public Vote vote(VoteDTO voteDTO) throws VoteOwnMovieException, RuntimeException {

        Movie movie = movieService.findMovieById(voteDTO.getMovieId());
        UserMovieKey userMovieKey = new UserMovieKey(voteDTO.getUserId(), voteDTO.getMovieId());
        Vote possibleExistingVote = findVote(userMovieKey);
        Vote vote = voteConverter.convert(voteDTO);

        if (voteDTO.getUserId().equals(movieService.findMovieById(voteDTO.getMovieId()).getUser().getId())) {
            throw new RuntimeException("You cannot vote for a Movie you have submitted");
        }
        if (possibleExistingVote == null & movie.getUser() != userService.findUser(voteDTO.getUserId())) {

            if (voteDTO.getUserVote() == UserVote.UPVOTE) {
                movie.setLikes(movie.getLikes() + 1);
                voteRepository.save(vote);
            } else if (voteDTO.getUserVote() == UserVote.DOWNVOTE) {
                movie.setDislikes(movie.getDislikes() + 1);
                voteRepository.save(vote);
            }

        } else if (possibleExistingVote != null & voteDTO.getUserVote() != possibleExistingVote.getUserVote()) {


            if (possibleExistingVote.getUserVote() == UserVote.UPVOTE) {
                movie.setLikes(movie.getLikes() - 1);
                voteRepository.delete(possibleExistingVote);
                movie.setDislikes(movie.getDislikes() + 1);
                voteRepository.save(vote);
            } else if (possibleExistingVote.getUserVote() == UserVote.DOWNVOTE) {
                movie.setDislikes(movie.getDislikes() - 1);
                voteRepository.delete(possibleExistingVote);
                movie.setLikes(movie.getLikes() + 1);
                voteRepository.save(vote);
            }

        }

        return vote;
    }
}
