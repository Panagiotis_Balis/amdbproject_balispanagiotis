package com.learning.academy.amdbProject.controller;


import com.learning.academy.amdbProject.domain.Vote;
import com.learning.academy.amdbProject.dto.VoteDTO;
import com.learning.academy.amdbProject.exceptions.Errors.VoteError;
import com.learning.academy.amdbProject.exceptions.VoteNotFoundException;
import com.learning.academy.amdbProject.service.VoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;

@RestController
@RequestMapping("/api")
public class VoteController {
    @Autowired
    private VoteService voteService;

    @PostMapping(path = "/votes/vote", consumes = "application/json;odata=verbose", produces = "application/json;odata=verbose")
    public void addVote(@RequestBody VoteDTO voteDTO) {
        voteService.vote(voteDTO);
    }

    @PostMapping(path = "/votes/deleteVote", consumes = "application/json;odata=verbose", produces = "application/json;odata=verbose")
    public void deleteVote(@RequestBody VoteDTO voteDTO){
        voteService.delete(voteDTO);
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity handleException(RuntimeException ex) {

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(ex.getMessage());
    }
}
