package com.learning.academy.amdbProject.controller;

import com.learning.academy.amdbProject.domain.Movies;
import com.learning.academy.amdbProject.dto.MovieDTO;
import com.learning.academy.amdbProject.enums.SortingOrder;
import com.learning.academy.amdbProject.enums.SortingType;
import com.learning.academy.amdbProject.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class MovieController {


    @Autowired
    private MovieService movieService;


    @GetMapping("/movies")
    ResponseEntity<Movies> getMovies(@RequestParam(value = "sortingType", required = false) SortingType sortingType,
                                     @RequestParam(value = "sortingOrder", required = false) SortingOrder sortingOrder) {
        //test 2
        //Implementation goes here
        //ieruhfieurhugiferf
        Movies movies = movieService.findMovies(sortingType, sortingOrder);
        return ResponseEntity.status(HttpStatus.OK).body(movies);
    }

    @GetMapping("/movies/{id}")
    ResponseEntity<MovieDTO> getMovie(@PathVariable(required=false)Long id){
        MovieDTO movieDTO = movieService.findMovieDto(id);
        return ResponseEntity.status(HttpStatus.OK).body(movieDTO);
    }

    @GetMapping("/users/{id}/movies")
    ResponseEntity<Movies> getMoviesByUser(@PathVariable(required = false) Long id,
                                           @RequestParam(value = "sortingType", required = false) SortingType sortingType,
                                           @RequestParam(value = "sortingOrder", required = false) SortingOrder sortingOrder) {
        Movies movies = movieService.findMoviesByUser(id, sortingType, sortingOrder);
        System.out.println("delete this comment");
        return ResponseEntity.status(HttpStatus.OK).body(movies);
    }

    @PostMapping(path = "/movies", consumes = "application/json;odata=verbose", produces = "application/json;odata=verbose")
    ResponseEntity<MovieDTO> addMMovie(@RequestBody MovieDTO movieDTO) {
        MovieDTO movie = movieService.saveMovie(movieDTO);
        return ResponseEntity.status(HttpStatus.OK).body(movie);
    }


}


