package com.learning.academy.amdbProject.controller;

import com.learning.academy.amdbProject.dto.UserDTO;
import com.learning.academy.amdbProject.dto.UserRegisterDTO;
import com.learning.academy.amdbProject.dto.UserSignInDTO;
import com.learning.academy.amdbProject.service.MovieService;
import com.learning.academy.amdbProject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class UserController {


    @Autowired
    private UserService userService;


    @PostMapping(path = "/users/register", consumes = "application/json;odata=verbose", produces = "application/json;odata=verbose")
    public void addUser(@RequestBody UserRegisterDTO userRegisterDTO) {
        userService.saveUser(userRegisterDTO);
    }

    @PostMapping(path = "/users/signIn", consumes = "application/json;odata=verbose", produces = "application/json;odata=verbose")
    public UserDTO signInUser(@RequestBody UserSignInDTO userSignInDTO) {

        return userService.signInUser(userSignInDTO);
    }




}
