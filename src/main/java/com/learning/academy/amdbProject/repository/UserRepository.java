package com.learning.academy.amdbProject.repository;

import com.learning.academy.amdbProject.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {

    User findUserById(Long id);

    List<User> findAll();

    User save(User user);

    User findUserByUsername(String userName);



}
