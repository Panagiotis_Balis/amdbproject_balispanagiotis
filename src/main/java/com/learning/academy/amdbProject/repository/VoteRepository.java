package com.learning.academy.amdbProject.repository;

import com.learning.academy.amdbProject.domain.Movie;
import com.learning.academy.amdbProject.domain.User;
import com.learning.academy.amdbProject.domain.UserMovieKey;
import com.learning.academy.amdbProject.domain.Vote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface VoteRepository extends JpaRepository<Vote, UserMovieKey> {
    List<Vote> findAll();
    Vote save(Vote vote);
    Optional<Vote> findById(UserMovieKey userMovieKey);
}
