package com.learning.academy.amdbProject.repository;

import com.learning.academy.amdbProject.domain.Movie;
import com.learning.academy.amdbProject.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {


    Movie save(Movie movie);

    List<Movie> findAllByOrderByCreatedAtDesc();

    List<Movie> findAllByOrderByLikesDesc();

    List<Movie> findAllByOrderByDislikesDesc();

    List<Movie> findAllByOrderByLikesAsc();

    List<Movie> findAllByOrderByCreatedAtAsc();

    List<Movie> findAllByOrderByDislikesAsc();

    List<Movie> findAllMoviesByUser_IdOrderByCreatedAtAsc(Long id);

    List<Movie> findMoviesByUser_IdOrderByLikesDesc(Long id);

    List<Movie> findMoviesByUser_IdOrderByDislikesDesc(Long id);

    List<Movie> findMoviesByUser_IdOrderByLikesAsc(Long id);

    List<Movie> findMoviesByUser_IdOrderByDislikesAsc(Long id);

    List<Movie> findMovieByUserOrderByCreatedAtDesc(User user);

}
