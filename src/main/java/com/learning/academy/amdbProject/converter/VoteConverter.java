package com.learning.academy.amdbProject.converter;

import com.learning.academy.amdbProject.domain.UserMovieKey;
import com.learning.academy.amdbProject.domain.Vote;
import com.learning.academy.amdbProject.dto.VoteDTO;
import com.learning.academy.amdbProject.service.MovieService;
import com.learning.academy.amdbProject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class VoteConverter {
    @Autowired
    UserService userService;
    @Autowired
    MovieService movieService;


    public Vote convert(VoteDTO voteDTO) {
        UserMovieKey userMovieKey = new UserMovieKey(voteDTO.getUserId(), voteDTO.getMovieId());
        Vote vote = new Vote(userMovieKey, movieService.findMovieById(voteDTO.getMovieId()),
                userService.findUser(voteDTO.getUserId()), voteDTO.getUserVote());
        return vote;
    }



}
