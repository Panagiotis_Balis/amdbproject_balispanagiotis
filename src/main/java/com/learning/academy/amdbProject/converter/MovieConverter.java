package com.learning.academy.amdbProject.converter;

import com.learning.academy.amdbProject.domain.Movie;
import com.learning.academy.amdbProject.dto.MovieDTO;
import com.learning.academy.amdbProject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MovieConverter {

    @Autowired
    private UserConverter userConverter;

    @Autowired
    private UserService userservice;


    public MovieDTO convert(Movie movie) {
        MovieDTO movieDTO = new MovieDTO();
        movieDTO.setCreatedAt(movie.getCreatedAt());
        movieDTO.setDescription(movie.getDescription());
        movieDTO.setDislikes(movie.getDislikes());
        movieDTO.setId(movie.getId());
        movieDTO.setLikes(movie.getLikes());
        movieDTO.setTitle(movie.getTitle());
        movieDTO.setUserDTO(userConverter.convert(movie.getUser()));

        return movieDTO;
    }

    public Movie convert(MovieDTO movieDTO) {
        Movie movie = new Movie();
        movie.setDescription(movieDTO.getDescription());
        movie.setTitle(movieDTO.getTitle());
        movie.setCreatedAt(null);
        movie.setLikes(0);
        movie.setDislikes(0);
        movie.setVersion(0L);
        movie.setUser(userservice.findUser(movieDTO.getUserDTO().getId()));
        return movie;
    }


}
