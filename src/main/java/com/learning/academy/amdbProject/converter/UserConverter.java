package com.learning.academy.amdbProject.converter;

import com.learning.academy.amdbProject.domain.User;
import com.learning.academy.amdbProject.dto.UserDTO;
import com.learning.academy.amdbProject.dto.UserRegisterDTO;
import com.learning.academy.amdbProject.dto.UserSignInDTO;
import com.learning.academy.amdbProject.enums.UserRole;
import com.learning.academy.amdbProject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserConverter {

    @Autowired
    UserService userService;

    public UserDTO convert(User user){
        UserDTO userDTO = new UserDTO();
        userDTO.setName(user.getUsername());
        userDTO.setId(user.getId());

        return userDTO;
    }

    public User convert(UserDTO userDTO){
        User user = new User();
        user.setName(userDTO.getName());
        user.setId(userDTO.getId());

        return user;
    }

    public  User convert(UserRegisterDTO userRegisterDTO){
        User user = new User();
        user.setName(userRegisterDTO.getName());
        user.setUsername(userRegisterDTO.getUser_name());
        user.setPassword(userRegisterDTO.getPassword());
        user.setUserRole(UserRole.USER);

        return user;
    }

    public UserDTO convert(UserSignInDTO userSignInDTO){
        User user = userService.findUserByUserName(userSignInDTO.getUsername());
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setName(user.getName());
        return userDTO;
    }
}
