-- https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-sql.html
-- https://docs.spring.io/spring-boot/docs/current/reference/html/howto-database-initialization.html
-- IN THIS FILE WE CAN WRITE AN SQL SCRIPT CONTAINING:
-- SCHEMA, TABLE AND DATA MANIPULATION QUERIES
-- TO BE EXECUTED AUTOMATICALLY DURING THE INITIALIZATION OF THE APPLICATION
-- AND AFTER THE CREATION OF SCHEMA AND TABLES BY Hibernate
-- IF spring.jpa.hibernate.ddl-auto IS SET TO create OR create-drop
-- IT IS A Hibernate feature (nothing to do with Spring)

-- INSERT INTO USER (name, user_name, password,user_role) VALUES ('name', 'username', 'password', 'USER');
-- INSERT INTO USER (name, user_name, password,user_role) VALUES ('name2', 'username2', 'password2', 'USER');
-- INSERT INTO USER (name, user_name, password,user_role) VALUES ('name3', 'username3', 'password3', 'USER');
-- INSERT INTO USER (name, user_name, password,user_role) VALUES ('nam4', 'username4', 'password4', 'USER');

-- INSERT INTO USER (id, firstname, lastname, email, username, password, role) VALUES ('124', 'Jane', 'Doe', 'jane@mail.com', 'janeD', 'pass1234', 'ROLE_ADMIN');

-- INSERT INTO MOVIE (user_id,movie_title,movie_description,date_of_publication,number_of_likes,number_of_dislikes) VALUES ('1', 'movie1','description1','1734','2','10');
-- INSERT INTO MOVIE (user_id,movie_title,movie_description,date_of_publication,number_of_likes,number_of_dislikes) VALUES ( '2', 'movie2','description2','1874','3','7');
-- INSERT INTO MOVIE (user_id,movie_title,movie_description,date_of_publication,number_of_likes,number_of_dislikes) VALUES ('3', 'movie3','description3','1834','4','3');
-- INSERT INTO MOVIE (user_id,movie_title,movie_description,date_of_publication,number_of_likes,number_of_dislikes) VALUES ('4', 'movie4','description4','2034','5','1');

-- INSERT INTO VOTE (user_id,movie_id,user_vote) VALUES ('1','1','LIKE');
-- INSERT INTO VOTE (user_id,movie_id,user_vote) VALUES ('1','2','DISLIKE');
-- INSERT INTO VOTE (user_id,movie_id,user_vote) VALUES ('1','3','LIKE');
-- INSERT INTO VOTE (user_id,movie_id,user_vote) VALUES ('1','4','LIKE');
-- INSERT INTO VOTE (user_id,movie_id,user_vote) VALUES ('2','1','LIKE');
-- INSERT INTO VOTE (user_id,movie_id,user_vote) VALUES ('2','2','DISLIKE');
-- INSERT INTO VOTE (user_id,movie_id,user_vote) VALUES ('2','3','DISLIKE');
-- INSERT INTO VOTE (user_id,movie_id,user_vote) VALUES ('2','4','LIKE');
-- INSERT INTO VOTE (user_id,movie_id,user_vote) VALUES ('3','1','LIKE');
-- INSERT INTO VOTE (user_id,movie_id,user_vote) VALUES ('3','1','LIKE');
-- INSERT INTO VOTE (user_id,movie_id,user_vote) VALUES ('4','1','LIKE');
-- INSERT INTO VOTE (user_id,movie_id,user_vote) VALUES ('4','2','DISLIKE');

--USER
INSERT INTO user (user_id,user_name,password,user_role) VALUES (1,'Spock','pass1234','USER');
INSERT INTO user (user_id,user_name,password,user_role) VALUES (2,'Princess Amidala','pass1234','USER');
INSERT INTO user (user_id,user_name,password,user_role) VALUES (3,'Picard','pass1234','USER');
INSERT INTO user (user_id,user_name,password,user_role) VALUES (4,'Count Dooku','pass1234','USER');

--MOVIE
INSERT INTO movie (movie_id,movie_title,movie_description,user_id,number_of_likes,number_of_dislikes,version, created_at) VALUES (1,'Batman: The dark knight rises','I am Batman',1,2,0,0, '2019-01-01');
INSERT INTO movie (movie_id,movie_title,movie_description,user_id,number_of_likes,number_of_dislikes,version, created_at) VALUES (2,'Superman: The man of steel','I am Superman',2,1,1,0, '2019-02-02');
INSERT INTO movie (movie_id,movie_title,movie_description,user_id,number_of_likes,number_of_dislikes,version, created_at) VALUES (3,'Spiderman Homecoming','I am Spider-man',3,1,1,0, '2019-03-03');
INSERT INTO movie (movie_id,movie_title,movie_description,user_id,number_of_likes,number_of_dislikes,version, created_at) VALUES (4,'Star Trek: The final frontier','Live long and prosper',1,1,1,0, '2019-04-04');
INSERT INTO movie (movie_id,movie_title,movie_description,user_id,number_of_likes,number_of_dislikes,version, created_at) VALUES (5,'Star Wars: Empire strikes back','Luke you are my son',2,1,1,0, '2019-05-05');
INSERT INTO movie (movie_id,movie_title,movie_description,user_id,number_of_likes,number_of_dislikes,version, created_at) VALUES (6,'The legend of Zoro','Z',3,1,1,0, '2019-06-06');
INSERT INTO movie (movie_id,movie_title,movie_description,user_id,number_of_likes,number_of_dislikes,version, created_at) VALUES (7,'Casa de pappel','Dali masks and red suits',3,0,3,0, '2019-07-07');

--VOTE
INSERT INTO vote (user_id,movie_id,vote_type) VALUES (4,1,'UPVOTE');
INSERT INTO vote (user_id,movie_id,vote_type) VALUES (4,2,'UPVOTE');
INSERT INTO vote (user_id,movie_id,vote_type) VALUES (4,3,'UPVOTE');
INSERT INTO vote (user_id,movie_id,vote_type) VALUES (4,4,'UPVOTE');
INSERT INTO vote (user_id,movie_id,vote_type) VALUES (4,5,'UPVOTE');
INSERT INTO vote (user_id,movie_id,vote_type) VALUES (4,6,'UPVOTE');
INSERT INTO vote (user_id,movie_id,vote_type) VALUES (1,3,'DOWNVOTE');
INSERT INTO vote (user_id,movie_id,vote_type) VALUES (1,6,'UPVOTE');
INSERT INTO vote (user_id,movie_id,vote_type) VALUES (2,1,'UPVOTE');
INSERT INTO vote (user_id,movie_id,vote_type) VALUES (2,2,'DOWNVOTE');
INSERT INTO vote (user_id,movie_id,vote_type) VALUES (2,3,'DOWNVOTE');
INSERT INTO vote (user_id,movie_id,vote_type) VALUES (2,4,'DOWNVOTE');
INSERT INTO vote (user_id,movie_id,vote_type) VALUES (2,5,'DOWNVOTE');
INSERT INTO vote (user_id,movie_id,vote_type) VALUES (2,6,'DOWNVOTE');
INSERT INTO vote (user_id,movie_id,vote_type) VALUES (3,6,'DOWNVOTE');

-- INSERT INTO MOVIE (firstname, lastname) VALUES ('John', 'Steinbeck');
-- INSERT INTO MOVIE (firstname, lastname) VALUES ('Alexandros', 'Papadiamantis');
-- INSERT INTO MOVIE (firstname, lastname) VALUES ('Nikos', 'Kazantzakis');
-- -- INSERT INTO AUTHOR (firstname, lastname) VALUES ('Angelos', 'Terzakis');
--
-- INSERT INTO BOOK (book_title, publication_year, author_id, book_category) VALUES ('War and Peace', '1867', '1', 'LITERARY_REALISM');
-- INSERT INTO BOOK (book_title, publication_year, author_id, book_category) VALUES ('The Grapes of Wrath', '1939', '2', 'DRAMA');
-- INSERT INTO BOOK (book_title, publication_year, author_id, book_category) VALUES ('The Murderess', '1903', '3', 'DRAMA');
-- INSERT INTO BOOK (book_title, publication_year, author_id, book_category) VALUES ('Captain Michalis', '1950', '4', 'DRAMA');
-- INSERT INTO BOOK (book_title, publication_year, author_id, book_category) VALUES ('Report to Greco', '1965', '4', 'DRAMA');
-- INSERT INTO BOOK (book_title, publication_year, author_id, book_category) VALUES ('The Last Temptation', '1960', '4', 'DRAMA');
-- INSERT INTO BOOK (book_title, publication_year, author_id, book_category) VALUES ('Christ Recrucified', '1954', '4', 'DRAMA');
-- INSERT INTO BOOK (book_title, publication_year, author_id, book_category) VALUES ('Princess Isambo', '1945', '5', 'DRAMA');

-- insert into BOOK(book_id, book_title,publication_year,author_id,book_category) values ('fbe25e54-645f-4ede-829b-d28f3a29b2dc','War and Peace','1867','1','LITERARY_REALISM');
-- insert into BOOK(book_id, book_title,publication_year,author_id,book_category) values ('a6fe1574-04de-44d6-ae2c-9f43fc2f42f1','The Grapes of Wrath','1939','2','DRAMA');
-- insert into BOOK(book_id, book_title,publication_year,author_id,book_category) values ('2cfc4209-c32e-4a76-8b38-36b8ce5b3ef5','The Murderess','1903','3','DRAMA');
-- insert into BOOK(book_id, book_title,publication_year,author_id,book_category) values ('534d9f04-1c29-4b68-91a1-902b75c982b4','Captain Michalis','1950','4','DRAMA');
-- insert into BOOK(book_id, book_title,publication_year,author_id,book_category) values ('d7e7b6ed-fdae-424e-abc5-1718d82b64b8','Report to Greco','1965','4','DRAMA');
-- insert into BOOK(book_id, book_title,publication_year,author_id,book_category) values ('23b20325-fc09-43c0-b69b-64c47f73988d','The Last Temptation','1960','4','DRAMA');
-- insert into BOOK(book_id, book_title,publication_year,author_id,book_category) values ('6d002b7e-f985-477b-8320-b71ad7d7713a','Christ Recrucified','1954','4','DRAMA');
-- insert into BOOK(book_id, book_title,publication_year,author_id,book_category) values ('ba1ba1f3-bbdc-46a5-b279-3a013a4f5ec7','Princess Isambo','1945','5','DRAMA');